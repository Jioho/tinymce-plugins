const babel = require('rollup-plugin-babel') // ES6转ES5插件
const commonjs = require('rollup-plugin-commonjs') // 将CommonJS模块转换成ES6，防止他们在Rollup中失效;
const { uglify } = require('rollup-plugin-uglify') // js压缩
const serve = require('rollup-plugin-serve') // serve服务
const livereload = require('rollup-plugin-livereload') // 热更新
const postcss = require('rollup-plugin-postcss')
const copy = require('rollup-plugin-copy')
const clear = require('rollup-plugin-clear')

/**
 * 获取不同配置对应的插件
 * @param {*} isDev 是否开发环境
 * @param {*} isFirst 是否第一个入口
 * @param {*} config 拓展配置
 * @returns
 */
module.exports = function (isDev, isFirst, config = {}) {
  let plugins = [
    postcss({
      extensions: ['.less'], // 编译.less 文件
      extract: true,
      modules: false,
      inject: false, // 核心，不要在 html 插入css代码，因为 tinymce 有自己引入css的一套方法
      minimize: !config.unUglify
    }),
    commonjs(),
    babel({
      babelrc: false, //不设置.babelrc文件;
      exclude: 'node_modules/**',
      presets: ['es2015-rollup', 'stage-0'], //转ES5的插件;
      plugins: ['transform-class-properties'] //转换静态类属性以及属性声明的属性初始化语法
    })
  ]

  if (isDev) {
    // 只有第一个才需要serve
    isFirst &&
      plugins.push(
        serve({
          open: true, // 自动打开浏览器
          verbose: true, // 在终端输出打开的链接
          contentBase: config.srcPath || '', // 项目入口
          openPage: '/index.html', // 默认打开的页面
          port: '8080' // 端口号
        })
      )

    // dev 都加热更新
    plugins.push(livereload())
  } else {
    !config.unClear &&
      config.output &&
      plugins.push(
        // 清除插件
        clear({
          targets: [config.output]
        })
      )

    if (!config.unUglify) {
      // 打包才压缩js
      plugins.push(uglify())
    }
  }

  // 判断是否需要复制目录
  if (config.copyAssets) {
    console.log('copy', config.assetsPath, config.output)
    plugins.push(
      copy({
        targets: [
          {
            src: config.assetsPath,
            dest: config.output
          }
        ]
      })
    )
  }

  return plugins
}
