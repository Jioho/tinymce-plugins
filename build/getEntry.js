const path = require('path')
const fs = require('fs')

module.exports = function (entry, isDev) {
  if (!entry) {
    console.log('入口目录不存在', entry)
    process.exit(0)
  }

  try {
    let stat = fs.statSync(entry)

    if (!stat.isDirectory()) {
      console.log('入口目录不存在', entry)
      process.exit(0)
    }
  } catch (e) {
    console.log('入口目录不存在', entry)
    process.exit(0)
  }

  let routes = []

  fs.readdirSync(entry).forEach(function (item, index) {
    let pluginFolder = path.join(entry, item)
    let floderStat = fs.statSync(pluginFolder)

    // 这里判断是否插件目录
    if (floderStat.isDirectory() && item.indexOf('my_') !== -1) {
      try {
        let plugin = path.join(pluginFolder, 'plugin.js')
        let pluginStat = fs.statSync(plugin)
        if (pluginStat && pluginStat.isFile()) {
          routes.push({
            path: plugin,
            floder: pluginFolder,
            name: item,
            assets: false,
          })

          if (!isDev) {
            let assets = path.join(pluginFolder, 'assets')
            let assetsStat = fs.statSync(assets)
            if (assetsStat.isDirectory()) {
              routes[routes.length - 1].assets = true
            }
          }
        }
      } catch (e) {}
    }
  })

  return routes
}
