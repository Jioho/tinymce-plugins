const path = require('path')

const rootPath = path.resolve(__dirname, '../')
const pluginsPath = path.resolve(__dirname, '../src/js/tinymce/plugins/')
const srcPath = path.resolve(__dirname, '../src/')

const isDev = process.env.NODE_ENV == 'development'

const getEntry = require('./getEntry')
const getPlugins = require('./getPlugins')

function run() {
  let config = []
  let isFirst = true
  getEntry(pluginsPath, isDev).forEach(item => {
    let _distPath = path.join(rootPath, 'dist', item.name)
    let _output = [
      {
        format: 'iife',
        file: path.join(item.floder, 'plugin.min.js') // 基础目录，打包在 plugins
      }
    ]
    if (!isDev) {
      _output.push({
        format: 'iife',
        file: path.join(_distPath, 'plugin.min.js') // 打包目录
      })
    }

    config.push({
      input: item.path,
      output: _output,
      plugins: getPlugins(isDev, isFirst, {
        srcPath,
        copyAssets: item.assets,
        assetsPath: 'src/js/tinymce/plugins/' + item.name + '/assets', // 复制目录需要相对目录
        output: 'dist/' + item.name
      })
    })

    !isDev &&
      config.push({
        input: item.path,
        output: {
          format: 'iife',
          file: path.join(_distPath, 'plugin.js') // 打包一个不压缩的
        },
        plugins: getPlugins(isDev, isFirst, { srcPath, unUglify: true, copyAssets: false, unClear: true })
      })

    isFirst = false
  })

  return config
}

export default run()
